//
//  iOSApplication.cpp
//  MyGdxTest
//
//  Created by Vasily on 06.10.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "iOSApplication.h"
#include "esUtil.h"
#include "Toonel.h"
#include "SimpleTexture2D.h"
#include "SpriteSample.h"
#include "AnimationTest.h"

#define SELECTED_SCREEN AnimationTest

ES_NAMESPACE_BEGIN
    
iOSApplication::iOSApplication() {
    
}

iOSApplication::~iOSApplication() {
    
}

void iOSApplication::create(ESContext* context) {
    mContext = new Context(context);
    mScreen = new SELECTED_SCREEN();
    mScreen->create(mContext);
}

void iOSApplication::resize() {
    
}

void iOSApplication::render(float deltaTime) {
    mScreen->render(deltaTime);
}

void iOSApplication::pause() {
    mScreen->pause();
}

void iOSApplication::resume() {
    mScreen->resume();
}

void iOSApplication::dispose() {
    delete mScreen;
}

ES_NAMESPACE_END
//
//  main.m
//  ESFramework
//
//  Created by Vasily on 13.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ESFAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ESFAppDelegate class]));
    }
}

//
//  ESFAppDelegate.h
//  ESFramework
//
//  Created by Vasily on 13.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ESFViewController;

@interface ESFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ESFViewController *viewController;

@end

//
//  ESFViewController.h
//  ESFramework
//
//  Created by Vasily on 13.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAGLView.h"

@interface ESFViewController : UIViewController {
    EAGLView *glView_;
}

@property (nonatomic, retain) IBOutlet EAGLView *glView;

@end

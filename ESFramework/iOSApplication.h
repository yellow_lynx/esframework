//
//  iOSApplication.h
//  MyGdxTest
//
//  Created by Vasily on 06.10.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __MyGdxTest__iOSApplication__
#define __MyGdxTest__iOSApplication__

#include "MyGL.h"

ES_NAMESPACE_BEGIN
    
class iOSApplication : public GLApplication {
private:
    // ESContext* mContext;
    GLuint mShaderProgram;
    
    int mPositionLocation;
    int mTextCoodLocation;
    int mSamplerLocation;
    
    //toonel
    int mTimeUniformLocation;
    int mResolutionUniformLocation;
    
    //int m
    
    Texture* mTexture;
    float mTime;
    
    Context* mContext;
    Screen* mScreen;
    
public:
    iOSApplication();
    virtual void create(ESContext*);
    virtual void resize();
    virtual void render(float);
    virtual void pause();
    virtual void resume();
    virtual void dispose();
    virtual ~iOSApplication();
};

ES_NAMESPACE_END

#endif /* defined(__MyGdxTest__iOSApplication__) */

//
//  GLApplication.h
//  MyGdxTest
//
//  Created by Vasily on 06.10.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __MyGdxTest__GLApplication__
#define __MyGdxTest__GLApplication__

#include "MyGL.h"

ES_NAMESPACE_BEGIN
    
class GLApplication {
public:
    virtual void create(ESContext*) = 0;
    virtual void resize() = 0;
    virtual void render(float) = 0;
    virtual void pause() = 0;
    virtual void resume() = 0;
    virtual void dispose() = 0;
};

ES_NAMESPACE_END

#endif /* defined(__MyGdxTest__GLApplication__) */

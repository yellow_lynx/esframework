//
//  Texture.h
//  ESFramework
//
//  Created by Vasily on 14.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__Texture__
#define __ESFramework__Texture__

#include <iostream>
#include "MyGL.h"

ES_NAMESPACE_BEGIN
    
class Texture {
private:
    int mTextureId;
    char* mFileName;
    
    int mWidth;
    int mHeight;
    
    int mMinFilter;
    int mMagFilter;
    
public:
    Texture(const char* fileName);
    ~Texture();
    
    bool load();
    void reload();
    
    void setFilters(int minFilter, int magFilter);
    
    void bind();
    void dispose();
    
    int getTextureId();
    
    int getWidth();
    int getHeight();
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__Texture__) */

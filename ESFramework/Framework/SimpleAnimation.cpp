//
//  SimpleAnimation.cpp
//  ESFramework
//
//  Created by Vasily on 09.12.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "SimpleAnimation.h"

ES_NAMESPACE_BEGIN

#define min(x, y) x < y ? x : y

SimpleAnimation::SimpleAnimation(float frameDuration, int frameCount, TextureRegion** frames) {
    mDuration = frameDuration;
    mCount = frameCount;
    mFrames = frames;
}

TextureRegion* SimpleAnimation::getFrame(float stateTime, int mode) {
    int frameNumber = (int)(stateTime  / mDuration);
    
    if (mode == ES_ANIMATION_LOOPING) {
        frameNumber = min(mCount - 1, frameNumber);
    } else {
        frameNumber = frameNumber % mCount;
    }
    
    return mFrames[frameNumber];
}

ES_NAMESPACE_END
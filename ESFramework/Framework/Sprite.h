//
//  Sprite.h
//  ESFramework
//
//  Created by Vasily Laushkin on 18.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__Sprite__
#define __ESFramework__Sprite__

#include "MyGL.h"

ES_NAMESPACE_BEGIN

class Texture;
class Context;

class Sprite {
    Texture* mTexture;
    float mWidth;
    float mHeight;
    int mShaderProgram;
    
    int mPositionLocation;
    int mTextCoordLocation;
    int mSamplerLocation;
    int mProjMatrixLocation;
    
public:
    Sprite(const char* image, float width, float height);
    void draw(Context* context, float x, float y);
    ~Sprite();
};

ES_NAMESPACE_END



#endif /* defined(__ESFramework__Sprite__) */

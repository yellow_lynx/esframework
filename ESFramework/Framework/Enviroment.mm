//
//  Enviroment.cpp
//  MyGdxTest
//
//  Created by Vasily on 06.10.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "Enviroment.h"

ES_NAMESPACE_BEGIN

Enviroment* Enviroment::enviroment = 0;

const char* Enviroment::getAssetsPath() {
    NSString* path = [[NSBundle mainBundle] resourcePath];
    const char* result = [path cStringUsingEncoding:NSUTF8StringEncoding];
    return result;
}

const char* Enviroment::getPathForResource(const char* resource, const char* type) {
    
    NSString* res = [[NSString alloc] initWithCString:resource encoding:NSUTF8StringEncoding];
    NSString* tp = [[NSString alloc] initWithCString:type encoding:NSUTF8StringEncoding];
    
    NSString* result = [[NSBundle mainBundle] pathForResource:res ofType:tp];
    const char* cResult = [result cStringUsingEncoding:NSUTF8StringEncoding];
    [res release];
    [tp release];
    
    return cResult;
}

ES_NAMESPACE_END
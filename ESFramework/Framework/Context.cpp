//
//  Context.cpp
//  ESFramework
//
//  Created by Vasily on 17.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "Context.h"

ES_NAMESPACE_BEGIN

Context::Context(ESContext* context) {
    mESContext = context;
}

ESContext* Context::getESContext() {
    return mESContext;
}

ES_NAMESPACE_END
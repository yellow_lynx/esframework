//
//  SimpleAnimation.h
//  ESFramework
//
//  Created by Vasily on 09.12.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__SimpleAnimation__
#define __ESFramework__SimpleAnimation__

#include "MyGL.h"

ES_NAMESPACE_BEGIN

#define ES_ANIMATION_LOOPING 0
#define ES_ANIMATION_NONLOOPING 1

class SimpleAnimation {
    TextureRegion** mFrames;
    int mCount;
    float mDuration;
    
public:
    SimpleAnimation(float frameDuration, int frameCount, TextureRegion** frames);
    TextureRegion* getFrame(float statTime, int mode);
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__SimpleAnimation__) */

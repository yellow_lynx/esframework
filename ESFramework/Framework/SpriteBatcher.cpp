//
//  SpriteBatcher.cpp
//  ESFramework
//
//  Created by Vasily on 04.12.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "SpriteBatcher.h"

ES_NAMESPACE_BEGIN

SpriteBatcher::SpriteBatcher(Context* context, int maxSprites) {
    mContext = context;
    
    mVerticesBuffer = new float[maxSprites * 4 * 4];
    mBufferIndex = 0;
    mNumSprites = 0;
    
    int len = maxSprites * 6;
    mIndicesBuffer = new short[len];
    short j = 0;
    for (int i = 0; i < len; i +=6, j += 4) {
        mIndicesBuffer[i + 0] = (short) (j + 0);
        mIndicesBuffer[i + 1] = (short) (j + 1);
        mIndicesBuffer[i + 2] = (short) (j + 2);
        mIndicesBuffer[i + 3] = (short) (j + 2);
        mIndicesBuffer[i + 4] = (short) (j + 3);
        mIndicesBuffer[i + 5] = (short) (j + 0);
    }
}

SpriteBatcher::~SpriteBatcher() {
    glDeleteProgram(mShaderProgram);
}

void SpriteBatcher::begin(Texture* texture) {
    const char vShaderStr[] =
    "attribute vec4 a_position;                \n"
    "attribute vec2 a_texCoord;                \n"
    "uniform mat4 projMatrix;                  \n"
    "varying vec2 v_texCoord;                  \n"
    "void main()                               \n"
    "{                                         \n"
    "   gl_Position = projMatrix * a_position; \n"
    "   v_texCoord = a_texCoord;               \n"
    "}                                         \n";
    
    const char fShaderStr[] =
    "precision mediump float;                            \n"
    "varying vec2 v_texCoord;                            \n"
    "uniform sampler2D s_texture;                        \n"
    "void main()                                         \n"
    "{                                                   \n"
    "  gl_FragColor = texture2D( s_texture, v_texCoord );\n"
    "}                                                   \n";
    
    mShaderProgram = esLoadProgram(vShaderStr, fShaderStr);
    
    mPositionLocation = glGetAttribLocation(mShaderProgram, "a_position");
    mTextCoordLocation = glGetAttribLocation(mShaderProgram, "a_texCoord");
    mSamplerLocation = glGetUniformLocation(mShaderProgram, "s_texture");
    mProjMatrixLocation = glGetUniformLocation(mShaderProgram, "projMatrix");
    
    glActiveTexture(GL_TEXTURE0);
    texture->bind();
    mNumSprites = 0;
    mBufferIndex = 0;
}

void SpriteBatcher::drawSprite(float x, float y, float width, float height, TextureRegion* region) {
    float halfWidth = width / 2;
    float halfHeight = height / 2;
    float x1 = x - halfWidth;
    float y1 = y - halfHeight;
    float x2 = x + halfWidth;
    float y2 = y + halfHeight;
    
    mVerticesBuffer[mBufferIndex++] = x1;
    mVerticesBuffer[mBufferIndex++] = y1;
    mVerticesBuffer[mBufferIndex++] = region->getU1();
    mVerticesBuffer[mBufferIndex++] = region->getV2();
    
    mVerticesBuffer[mBufferIndex++] = x2;
    mVerticesBuffer[mBufferIndex++] = y1;
    mVerticesBuffer[mBufferIndex++] = region->getU2();
    mVerticesBuffer[mBufferIndex++] = region->getV2();
    
    mVerticesBuffer[mBufferIndex++] = x2;
    mVerticesBuffer[mBufferIndex++] = y2;
    mVerticesBuffer[mBufferIndex++] = region->getU2();
    mVerticesBuffer[mBufferIndex++] = region->getV1();
    
    mVerticesBuffer[mBufferIndex++] = x1;
    mVerticesBuffer[mBufferIndex++] = y2;
    mVerticesBuffer[mBufferIndex++] = region->getU1();
    mVerticesBuffer[mBufferIndex++] = region->getV1();
    
    mNumSprites++;
}

void SpriteBatcher::end() {
    ESMatrix projMatrix;
    esMatrixLoadIdentity(&projMatrix);
    esOrtho(&projMatrix, 0, mContext->getESContext()->width, 0, mContext->getESContext()->height, 0, 1);
    
    glUseProgram(mShaderProgram);
    
    glVertexAttribPointer(mPositionLocation, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), mVerticesBuffer);
    glVertexAttribPointer(mTextCoordLocation, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), &mVerticesBuffer[2]);
    
    glEnableVertexAttribArray(mPositionLocation);
    glEnableVertexAttribArray(mTextCoordLocation);
    
    glUniformMatrix4fv(mProjMatrixLocation, 1, GL_FALSE, (const GLfloat*)projMatrix.m);
    
    glUniform1i(mSamplerLocation, 0);
    glDrawElements(GL_TRIANGLES, mNumSprites * 6, GL_UNSIGNED_SHORT, mIndicesBuffer);
    
    glDisableVertexAttribArray(mPositionLocation);
    glDisableVertexAttribArray(mTextCoordLocation);
}

ES_NAMESPACE_END
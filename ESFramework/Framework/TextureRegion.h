//
//  TextureRegion.h
//  ESFramework
//
//  Created by Vasily on 04.12.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__TextureRegion__
#define __ESFramework__TextureRegion__

#include "MyGL.h"

ES_NAMESPACE_BEGIN

class Texture;

class TextureRegion {
    float u1, v1;
    float u2, v2;
    Texture* mTexture;
    
public:
    float getU1();
    float getV1();
    
    float getU2();
    float getV2();
    
    Texture* getTexture();
    
    TextureRegion(Texture* texture, float x, float y, float width, float height);
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__TextureRegion__) */

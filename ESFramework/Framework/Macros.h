//
//  Macros.h
//  ESFramework
//
//  Created by Vasily on 16.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef ESFramework_Macros_h
#define ESFramework_Macros_h

#define MYGL_NAMESPACE ESGL

#define ES_NAMESPACE_BEGIN      namespace MYGL_NAMESPACE {
#define ES_NAMESPACE_END        }

#define ES_USING_NAMESPACE      using namespace MYGL_NAMESPACE

#define iOSAPP(x) ((MYGL_NAMESPACE::iOSApplication*)x)
#define NEW_iOS_APPLICATION() new MYGL_NAMESPACE::iOSApplication()

#define ESLog esLogMessage

#endif

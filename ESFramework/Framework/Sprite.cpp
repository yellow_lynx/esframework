//
//  Sprite.cpp
//  ESFramework
//
//  Created by Vasily Laushkin on 18.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "Sprite.h"

ES_NAMESPACE_BEGIN

Sprite::Sprite(const char* image, float width, float height) {
    const char vShaderStr[] =
    "attribute vec4 a_position;                \n"
    "attribute vec2 a_texCoord;                \n"
    "uniform mat4 projMatrix;                  \n"
    "varying vec2 v_texCoord;                  \n"
    "void main()                               \n"
    "{                                         \n"
    "   gl_Position = projMatrix * a_position; \n"
    "   v_texCoord = a_texCoord;               \n"
    "}                                         \n";
    
    const char fShaderStr[] =
    "precision mediump float;                            \n"
    "varying vec2 v_texCoord;                            \n"
    "uniform sampler2D s_texture;                        \n"
    "void main()                                         \n"
    "{                                                   \n"
    "  gl_FragColor = texture2D( s_texture, v_texCoord );\n"
    "}                                                   \n";
    
    mShaderProgram = esLoadProgram(vShaderStr, fShaderStr);
    
    mTexture = new Texture(image);
    mTexture->load();
    mWidth = width;
    mHeight = height;
    
    mPositionLocation = glGetAttribLocation(mShaderProgram, "a_position");
    mTextCoordLocation = glGetAttribLocation(mShaderProgram, "a_texCoord");
    mSamplerLocation = glGetUniformLocation(mShaderProgram, "s_texture");
    mProjMatrixLocation = glGetUniformLocation(mShaderProgram, "projMatrix");
}

void Sprite::draw(Context* context, float x, float y) {
    GLfloat vVertices[] = { x,  y,  // Position 0
        0.0f,  0.0f,        // TexCoord 0
        x, y + mHeight,  // Position 1
        0.0f,  1.0f,        // TexCoord 1
        x + mWidth, y + mHeight,  // Position 2
        1.0f,  1.0f,        // TexCoord 2
        x + mWidth,  y,  // Position 3
        1.0f,  0.0f         // TexCoord 3
    };

    GLushort indices[] = { 0, 1, 2, 0, 3, 2 };
    
    ESMatrix projMatrix;
    esMatrixLoadIdentity(&projMatrix);
    esOrtho(&projMatrix, 0, context->getESContext()->width, context->getESContext()->height, 0, 0, 1);
    
    glUseProgram(mShaderProgram);
    
    glVertexAttribPointer(mPositionLocation, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), vVertices);
    glVertexAttribPointer(mTextCoordLocation, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), &vVertices[2]);
    
    glEnableVertexAttribArray(mPositionLocation);
    glEnableVertexAttribArray(mTextCoordLocation);
    
    glUniformMatrix4fv(mProjMatrixLocation, 1, GL_FALSE, (const GLfloat*)projMatrix.m);
    
    glActiveTexture(GL_TEXTURE0);
    mTexture->bind();
    glUniform1i(mSamplerLocation, 0);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices);
    
}

Sprite::~Sprite() {
    mTexture->dispose();
    delete mTexture;
}

ES_NAMESPACE_END
//
//  MyGL.h
//  MyGdxTest
//
//  Created by Vasily on 06.10.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef MyGdxTest_MyGL_h
#define MyGdxTest_MyGL_h

#define MYGL_ERROR(x) {}

#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

#include "esUtil.h"
#include "Macros.h"
#include "Context.h"
#include "Enviroment.h"
#include "Texture.h"
#include "GLApplication.h"
#include "Screen.h"
#include "Sprite.h"
#include "Vector2.h"
#include "TextureRegion.h"
#include "SpriteBatcher.h"
#include "SimpleAnimation.h"

#endif

//
//  Enviroment.h
//  MyGdxTest
//
//  Created by Vasily on 06.10.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __MyGdxTest__Enviroment__
#define __MyGdxTest__Enviroment__

#include <iostream>
#include "MyGL.h"

ES_NAMESPACE_BEGIN

class Enviroment {
    static Enviroment* enviroment;
    
    Enviroment() {
        
    }
    
public:
    
    static Enviroment* getInstance() {
        if (enviroment == NULL) {
            enviroment = new Enviroment();
        }
        
        return enviroment;
    }
    
    const char* getAssetsPath();
    const char* getPathForResource(const char* resource, const char* type);
    
};

ES_NAMESPACE_END

#endif /* defined(__MyGdxTest__Enviroment__) */

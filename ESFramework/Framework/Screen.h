//
//  Screen.h
//  ESFramework
//
//  Created by Vasily on 17.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef ESFramework_Screen_h
#define ESFramework_Screen_h

#include "MyGL.h"

ES_NAMESPACE_BEGIN

class Context;

class Screen {
public:
    virtual void create(Context*) = 0;
    virtual void resize() = 0;
    virtual void prepare() = 0;
    virtual void render(float) = 0;
    virtual void pause() = 0;
    virtual void resume() = 0;
    virtual ~Screen() = 0;
};

inline Screen::~Screen() {}

ES_NAMESPACE_END


#endif

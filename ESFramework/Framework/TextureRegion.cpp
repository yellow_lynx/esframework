//
//  TextureRegion.cpp
//  ESFramework
//
//  Created by Vasily on 04.12.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "TextureRegion.h"

ES_NAMESPACE_BEGIN

float TextureRegion::getU1() {
    return u1;
}

float TextureRegion::getV1() {
    return v1;
}

float TextureRegion::getU2() {
    return u2;
}

float TextureRegion::getV2() {
    return v2;
}

Texture* TextureRegion::getTexture() {
    return mTexture;
}

TextureRegion::TextureRegion(Texture* texture, float x, float y, float width, float height) {
    u1 = x / texture->getWidth();
    v1 = y / texture->getHeight();
    
    u2 = u1 + width / texture->getWidth();
    v2 = v1 + height / texture->getHeight();
    
    mTexture = texture;
}

ES_NAMESPACE_END
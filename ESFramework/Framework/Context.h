//
//  Context.h
//  ESFramework
//
//  Created by Vasily on 17.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__Context__
#define __ESFramework__Context__

#include <iostream>
#include "MyGL.h"

ES_NAMESPACE_BEGIN

class Context {
    ESContext* mESContext;
    
public:
    Context(ESContext* context);
    ESContext* getESContext();
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__Context__) */

//
//  Vector2.h
//  ESFramework
//
//  Created by Vasily Laushkin on 24.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__Vector2__
#define __ESFramework__Vector2__

#include <math.h>
#define Vec2f Vector2<float>
#define Vec2i Vector2<int>

#define PI 3.14f
#define TO_RADIANS(x) x * (1 / 180.0f) * PI
#define TO_DEGREES(x) x * (1 / PI) * 180.0f

ES_NAMESPACE_BEGIN

template <typename T>
class Vector2 {
    
public:
    
    T x;
    T y;
    
    Vector2(T x, T y) {
        this->x = x;
        this->y = y;
    }
    
    Vector2(const Vector2& vec) {
        this->x = vec.x;
        this->y = vec.y;
    }
    
    Vector2& add(const Vector2& vec) {
        this->x += vec.x;
        this->y += vec.y;
        
        return *this;
    }
    
    Vector2& sub(const Vector2& vec) {
        this->x -= vec.x;
        this->y -= vec.y;
        
        return *this;
    }
    
    Vector2& mul(const Vector2& vec) {
        this->x *= vec.x;
        this->y *= vec.y;
        
        return *this;
    }
    
    T len() {
        return (T)sqrt(x * x + y * y);
    }
    
    Vector2 cpy() {
        return *this;
    }
    
    Vector2& nor() {
        T length = len();
        if (length != 0) {
            this->x /= length;
            this->y /= length;
        }
        
        return *this;
    }

    T angle() {
        T angle = (T)TO_DEGREES((T) atan2(x, y));
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }

    Vector2& rotate(float angle) {
        T rad = TO_RADIANS(angle);
        T cs = cos(rad);
        T sn = sin(rad);
        
        T newX = this->x * cs - this->y * sn;
        T newY = this->x * sn - this->y * cs;
        
        this->x = newX;
        this->y = newY;
        
        return *this;
    }

    T dist(Vector2& vec) {
        T distX = this->x - vec.x;
        T distY = this->y - vec.y;
        return (T)sqrt(distX * distX + distY * distY);
    }
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__Vector2__) */

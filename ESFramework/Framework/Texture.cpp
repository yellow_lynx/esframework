//
//  Texture.cpp
//  ESFramework
//
//  Created by Vasily on 14.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "Texture.h"
#include "esUtil.h"

ES_NAMESPACE_BEGIN

Texture::Texture(const char* fileName) {
    this->mFileName = strdup(fileName);
}

Texture::~Texture() {
    
}

bool Texture::load() {
    
    // generate ID
    unsigned int textureIds[1] = {0};
    GLuint* pTextureIds = (GLuint*) textureIds;
    glGenTextures(1, pTextureIds);
    this->mTextureId = textureIds[0];
    
    int width, height;
    char* buffer = esLoadPNG(this->mFileName, &width, &height);
    mWidth = width;
    mHeight = height;
    
    if (buffer == NULL) {
        return false;
    }
    
    glBindTexture(GL_TEXTURE_2D, mTextureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
    setFilters(GL_NEAREST, GL_NEAREST);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    free(buffer);
    
    return true;
}

void Texture::reload() {
    load();
    bind();
    setFilters(mMinFilter, mMagFilter);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::setFilters(int minFilter, int magFilter) {
    this->mMinFilter = minFilter;
    this->mMagFilter = magFilter;
    
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
}

void Texture::bind() {
    glBindTexture(GL_TEXTURE_2D, mTextureId);
}

void Texture::dispose() {
    glBindTexture(GL_TEXTURE_2D, mTextureId);
    unsigned int textureIds[1] = {0};
    GLuint* pTextureIds = (GLuint*) textureIds;
    
    glDeleteTextures(1, pTextureIds);
}

int Texture::getTextureId() {
    return mTextureId;
}

int Texture::getWidth() {
    return mWidth;
}

int Texture::getHeight() {
    return mWidth;
}

ES_NAMESPACE_END
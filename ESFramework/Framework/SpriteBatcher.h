//
//  SpriteBatcher.h
//  ESFramework
//
//  Created by Vasily on 04.12.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__SpriteBatcher__
#define __ESFramework__SpriteBatcher__

#include "MyGL.h"

ES_NAMESPACE_BEGIN

template class Vec2f;
class Context;
class Texture;
class TextureRegion;

class SpriteBatcher {
    Context* mContext;
    
    int mShaderProgram;
    
    int mPositionLocation;
    int mTextCoordLocation;
    int mSamplerLocation;
    int mProjMatrixLocation;
    
    float* mVerticesBuffer;
    short* mIndicesBuffer;
    int mBufferIndex;
    int mNumSprites;
    
public:
    
    SpriteBatcher(Context* context, int maxSprites);
    ~SpriteBatcher();
    void begin(Texture* texture);
    void drawSprite(float x, float y, float width, float height, TextureRegion* region);
    void end();
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__SpriteBatcher__) */

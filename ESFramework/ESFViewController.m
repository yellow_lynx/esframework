//
//  ESFViewController.m
//  ESFramework
//
//  Created by Vasily on 13.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#import "ESFViewController.h"

@interface ESFViewController ()

@end

@implementation ESFViewController

@synthesize glView=glView_;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [glView_ startAnimation];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

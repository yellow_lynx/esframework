//
//  SimpleTexture2D.h
//  ESFramework
//
//  Created by Vasily on 17.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__SimpleTexture2D__
#define __ESFramework__SimpleTexture2D__

#include <iostream>
#include "MyGL.h"

ES_NAMESPACE_BEGIN

class Texture;

class SimpleTexture2D : public Screen {
    Context* mContext;
    
    GLuint mShaderProgram;
    
    int mPositionLocation;
    int mTextCoodLocation;
    int mSamplerLocation;
    
    Texture* mTexture;
    
public:
    virtual void create(Context*);
    virtual void resize();
    virtual void prepare();
    virtual void render(float);
    virtual void pause();
    virtual void resume();
    virtual ~SimpleTexture2D();
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__SimpleTexture2D__) */

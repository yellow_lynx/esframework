//
//  AnimationTest.h
//  ESFramework
//
//  Created by Vasily on 09.12.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__AnimationTest__
#define __ESFramework__AnimationTest__

#include "MyGL.h"

ES_NAMESPACE_BEGIN

class SpriteBatcher;
class Texture;
class TextureRegion;
class Sprite;
class SimpleAnimation;

class AnimationTest : public Screen {
    Context* mContext;
    Texture* mTexture;
    SpriteBatcher* mSpriteBatcher;
    
    TextureRegion** mFrames;
    SimpleAnimation* mAnimationManager;
    
    float mFrameWidth;
    float mFrameHeight;
    
    Sprite* mSprite;
    float stateTime;
    
public:
    virtual void create(Context*);
    virtual void resize();
    virtual void prepare();
    virtual void render(float);
    virtual void pause();
    virtual void resume();
    virtual ~AnimationTest();
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__AnimationTest__) */

//
//  Sprites.h
//  ESFramework
//
//  Created by Vasily Laushkin on 18.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__Sprites__
#define __ESFramework__Sprites__

#include "MyGL.h"

class Texture;
class Sprite;

ES_NAMESPACE_BEGIN

class SpriteSample : public Screen {
    Context* mContext;
    Sprite* mSprite1;
    Sprite* mSprite2;

public:
    virtual void create(Context*);
    virtual void resize();
    virtual void prepare();
    virtual void render(float);
    virtual void pause();
    virtual void resume();
    virtual ~SpriteSample();
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__Sprites__) */

//
//  Sprites.cpp
//  ESFramework
//
//  Created by Vasily Laushkin on 18.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "SpriteSample.h"

ES_NAMESPACE_BEGIN

void SpriteSample::create(Context* context) {
    mContext = context;
    mSprite1 = new Sprite("tex1", 100, 100);
    mSprite2 = new Sprite("tex3", 200, 200);
}

void SpriteSample::resize() {
    
}

void SpriteSample::prepare() {
    
}

void SpriteSample::render(float deltaTime) {
    glViewport ( 0, 0, mContext->getESContext()->width, mContext->getESContext()->height );
    glClear(GL_COLOR_BUFFER_BIT);
    
    mSprite1->draw(mContext, 150, 250);
    mSprite2->draw(mContext, 10, 10);
    
    mSprite1->draw(mContext, 60, 360);
}

void SpriteSample::pause() {
    
}

void SpriteSample::resume() {
    
}

SpriteSample::~SpriteSample() {
    delete mSprite1;
    delete mSprite2;
}

ES_NAMESPACE_END
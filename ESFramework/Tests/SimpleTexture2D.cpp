//
//  SimpleTexture2D.cpp
//  ESFramework
//
//  Created by Vasily on 17.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "SimpleTexture2D.h"

ES_NAMESPACE_BEGIN

void SimpleTexture2D::create(Context* context) {
    mContext = context;
    
    const char vShaderStr[] =
    "attribute vec4 a_position;   \n"
    "attribute vec2 a_texCoord;   \n"
    "varying vec2 v_texCoord;     \n"
    "void main()                  \n"
    "{                            \n"
    "   gl_Position = a_position; \n"
    "   v_texCoord = a_texCoord;  \n"
    "}                            \n";
    
    const char fShaderStr[] =
    "precision mediump float;                            \n"
    "varying vec2 v_texCoord;                            \n"
    "uniform sampler2D s_texture;                        \n"
    "void main()                                         \n"
    "{                                                   \n"
    "  gl_FragColor = texture2D( s_texture, v_texCoord );\n"
    "}                                                   \n";
    
    mShaderProgram = esLoadProgram(vShaderStr, fShaderStr);
    
    mTexture = new Texture("tex3");
    mTexture->load();
    
    mPositionLocation = glGetAttribLocation(mShaderProgram, "a_position");
    mTextCoodLocation = glGetAttribLocation(mShaderProgram, "a_texCoord");
    mSamplerLocation = glGetAttribLocation(mShaderProgram, "s_texture");
}

void SimpleTexture2D::resize() {
    
}

void SimpleTexture2D::prepare() {
    
}

void SimpleTexture2D::render(float) {
    GLfloat vVertices[] = { -1.0f,  1.0f, 0.0f,  // Position 0
        0.0f,  0.0f,        // TexCoord 0
        -1.0f, -1.0f, 0.0f,  // Position 1
        0.0f,  1.0f,        // TexCoord 1
        1.0f, -1.0f, 0.0f,  // Position 2
        1.0f,  1.0f,        // TexCoord 2
        1.0f,  1.0f, 0.0f,  // Position 3
        1.0f,  0.0f         // TexCoord 3
    };
    
    GLushort indices[] = { 0, 1, 2, 0, 2, 3 };
    
    glViewport ( 0, 0, mContext->getESContext()->width, mContext->getESContext()->height );
    glClear(GL_COLOR_BUFFER_BIT);
    
    glUseProgram(mShaderProgram);
    
    glVertexAttribPointer(mPositionLocation, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), vVertices);
    glVertexAttribPointer(mTextCoodLocation, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), &vVertices[3]);
    glEnableVertexAttribArray(mPositionLocation);
    glEnableVertexAttribArray(mTextCoodLocation);
    
    glActiveTexture(GL_TEXTURE0);
    mTexture->bind();
    glUniform1i(mSamplerLocation, 0);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices);
    
}

void SimpleTexture2D::pause() {
    
}

void SimpleTexture2D::resume() {
    
}

SimpleTexture2D::~SimpleTexture2D() {
    mTexture->dispose();
    glDeleteProgram(mShaderProgram);
    delete mTexture;
}

ES_NAMESPACE_END
//
//  AnimationTest.cpp
//  ESFramework
//
//  Created by Vasily on 09.12.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "AnimationTest.h"

ES_NAMESPACE_BEGIN

#define FRAMES_WIDTH 6
#define FRAMES_HEIGHT 5

#define DURATION_TIME 0.15f
#define MAX_TIME FRAMES_HEIGHT * FRAMES_WIDTH * DURATION_TIME

void AnimationTest::create(Context* context) {
    mContext = context;
    mTexture = new Texture("animation");
    mTexture->load();
    
    mFrameWidth = mTexture->getWidth() / FRAMES_WIDTH;
    mFrameHeight = mTexture->getHeight() / FRAMES_HEIGHT;
    
    mSpriteBatcher = new SpriteBatcher(mContext, 1);
    
    stateTime = 0;
    
    mFrames = new TextureRegion*[FRAMES_WIDTH * FRAMES_HEIGHT];
    for (int i = 0; i < FRAMES_WIDTH; i++) {
        for (int j = 0; j < FRAMES_HEIGHT; j++) {
            TextureRegion* frame = new TextureRegion(mTexture, i * mFrameWidth, j * mFrameHeight, mFrameWidth, mFrameHeight);
            mFrames[i * (FRAMES_WIDTH - 1) + j] = frame;
        }
    }
    
    mAnimationManager = new SimpleAnimation(DURATION_TIME, FRAMES_WIDTH * FRAMES_HEIGHT, mFrames);
}

void AnimationTest::resize() {
    
}

void AnimationTest::prepare() {
    
}

void AnimationTest::render(float deltaTime) {
    glViewport ( 0, 0, mContext->getESContext()->width, mContext->getESContext()->height );
    glClear(GL_COLOR_BUFFER_BIT);
    
    if (stateTime >= MAX_TIME) {
        stateTime = 0;
    }
    
    stateTime += deltaTime;
    
    TextureRegion* frame = mAnimationManager->getFrame(stateTime, ES_ANIMATION_LOOPING);
    
    mSpriteBatcher->begin(mTexture);
    
    mSpriteBatcher->drawSprite(mContext->getESContext()->width / 2, mContext->getESContext()->height / 2, mFrameWidth, mFrameHeight, frame);
    
    mSpriteBatcher->end();
}

void AnimationTest::pause() {
    
}

void AnimationTest::resume() {
    
}

AnimationTest::~AnimationTest() {

}

ES_NAMESPACE_END
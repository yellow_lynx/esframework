//
//  Toonel.cpp
//  ESFramework
//
//  Created by Vasily on 17.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#include "Toonel.h"

#define START_TIME 0
#define END_TIME 40

ES_NAMESPACE_BEGIN

void Toonel::create(Context* context) {
    mContext = context;
    
    const char vShaderStr[] =
    "attribute vec4 a_position;   \n"
    "void main()                  \n"
    "{                            \n"
    "   gl_Position = a_position; \n"
    "}                            \n";
    
    const char fShaderStr[] =
    "#ifdef GL_ES\n"
    "precision highp float;\n"
    "#endif\n"
    "\n"
    "uniform vec2 resolution;\n"
    "uniform float time;\n"
    "uniform sampler2D tex0;\n"
    "\n"
    "void main(void)\n"
    "{\n"
    "    vec2 p = -1.0 + 2.0 * gl_FragCoord.xy / resolution.xy;\n"
    "    vec2 uv;\n"
    "   \n"
    "    float a = atan(p.y,p.x);\n"
    "    float r = sqrt(dot(p,p));\n"
    "\n"
    "    uv.x = .75*time+.1/r;\n"
    "    uv.y = a/3.1416;\n"
    "\n"
    "    vec3 col =  texture2D(tex0,uv).xyz;\n"
    "\n"
    "    gl_FragColor = vec4(col*r,1.0);\n"
    "}\n";
    
    mTime = START_TIME;
    
    mShaderProgram = esLoadProgram(vShaderStr, fShaderStr);
    
    mTexture = new Texture("tex1");
    mTexture->load();
    
    mPositionLocation = glGetAttribLocation(mShaderProgram, "a_position");
    mSamplerLocation = glGetUniformLocation(mShaderProgram, "tex0");
    mResolutionLocation = glGetUniformLocation(mShaderProgram, "resolution");
    mTimeUniformLocation = glGetUniformLocation(mShaderProgram, "time");
}

void Toonel::resize() {
    
}

void Toonel::prepare() {
    
}

void Toonel::render(float deltaTime) {
    GLfloat vVertices[] = { -1.0f,  1.0f, 0.0f,  // Position 0
        -1.0f, -1.0f, 0.0f,  // Position 1
        1.0f, -1.0f, 0.0f,  // Position 2
        1.0f,  1.0f, 0.0f,  // Position 3
    };
    
    GLushort indices[] = { 0, 1, 2, 0, 2, 3 };
    
    glViewport(0, 0, mContext->getESContext()->width, mContext->getESContext()->height);
    glClear(GL_COLOR_BUFFER_BIT);
    glUseProgram(mShaderProgram);
    glVertexAttribPointer(mPositionLocation, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), vVertices);
    glEnableVertexAttribArray(mPositionLocation);
    
    glActiveTexture(GL_TEXTURE0);
    mTexture->bind();
    glUniform1i(mSamplerLocation, 0);
    glUniform1f(mTimeUniformLocation, mTime);
    glUniform2f(mResolutionLocation, mContext->getESContext()->width, mContext->getESContext()->height);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices);
    mTime += deltaTime;
    if (mTime > END_TIME) {
        mTime = START_TIME;
    }
}

void Toonel::pause() {
    
}

void Toonel::resume() {
    
}

Toonel::~Toonel() {
    mTexture->dispose();
    glDeleteProgram(mShaderProgram);
    delete mTexture;
}

ES_NAMESPACE_END
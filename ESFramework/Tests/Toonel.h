//
//  Toonel.h
//  ESFramework
//
//  Created by Vasily on 17.11.12.
//  Copyright (c) 2012 Vasily. All rights reserved.
//

#ifndef __ESFramework__Toonel__
#define __ESFramework__Toonel__

#include "MyGL.h"

ES_NAMESPACE_BEGIN

class Texture;

class Toonel : public Screen {
    Context* mContext;
    
    GLuint mShaderProgram;
    
    int mPositionLocation;
    int mSamplerLocation;
    int mResolutionLocation;
    int mTimeUniformLocation;
    
    Texture* mTexture;
    float mTime;
    
public:
    virtual void create(Context*);
    virtual void resize();
    virtual void prepare();
    virtual void render(float);
    virtual void pause();
    virtual void resume();
    virtual ~Toonel();
};

ES_NAMESPACE_END

#endif /* defined(__ESFramework__Toonel__) */

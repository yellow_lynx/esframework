#!/bin/bash

if [ -e "$1" ]; then
	cat $1 | awk '{print "\042"$0"OLOLO\042"}' | sed "s/OLOLO/\\\n/"
fi
